#pragma once

#pragma comment(lib, "user32.lib")
#pragma comment(lib, "advapi32.lib")

using namespace System;

namespace ShudownWrapper {
	public ref class ShudownWrapper
	{
	public:
		static bool GetPrivilege();
		static void ShutDown();
		static void Reboot();
		static void Lock();
	};
}

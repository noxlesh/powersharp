﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace ShutSarp
{
    public partial class MainForm : Form
    {
        public MainForm()
        {
            InitializeComponent();
            notifyIcon.Icon = Icon;
            notifyIcon.ContextMenuStrip = contextMenuStrip;
            ShowInTaskbar = false;
            Hide();
        }

        #region event subscribers
        private void ShutdownInit(object sender, EventArgs e)
        {
            if (ShudownWrapper.ShudownWrapper.GetPrivilege())
            {
                if (MessageBox.Show(this, "Are you shure you want to shutdown the system?", "Shutdown confirmation", MessageBoxButtons.YesNo) == DialogResult.Yes)
                    ShudownWrapper.ShudownWrapper.ShutDown();
            }
        }

        private void RebootInit(object sender, EventArgs e)
        {
            if (ShudownWrapper.ShudownWrapper.GetPrivilege())
            {
                if(MessageBox.Show(this, "Are you shure you want to reboot the system?", "Reboot confirmation", MessageBoxButtons.YesNo) == DialogResult.Yes)
                    ShudownWrapper.ShudownWrapper.Reboot();
            }
        }

        private void SleepInit(object sender, EventArgs e)
        {
            if (MessageBox.Show(this, "Are you shure you want to suspend the system?", "Suspend confirmation", MessageBoxButtons.YesNo) == DialogResult.Yes)
                Application.SetSuspendState(PowerState.Suspend, true, true);
        }

        private void HibernateInit(object sender, EventArgs e)
        {
            if (MessageBox.Show(this, "Are you shure you want to hibernate the system?", "Hibernate confirmation", MessageBoxButtons.YesNo) == DialogResult.Yes)
                Application.SetSuspendState(PowerState.Hibernate, true, true);
        }

        private void LockInit(object sender, EventArgs e)
        {
            ShudownWrapper.ShudownWrapper.Lock();
        }

        private void MainForm_FormClosing(object sender, FormClosingEventArgs e)
        {
            if (e.CloseReason == CloseReason.UserClosing)
            {
                e.Cancel = true;
                Hide();
            }
        }

        private void menuItemExit_Click(object sender, EventArgs e)
        {
            Application.Exit();
        }
        #endregion



    }
}

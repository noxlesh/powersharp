﻿namespace ShutSarp
{
    partial class MainForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(MainForm));
            this.notifyIcon = new System.Windows.Forms.NotifyIcon(this.components);
            this.contextMenuStrip = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.MenuItemShutdown = new System.Windows.Forms.ToolStripMenuItem();
            this.menuItemReboot = new System.Windows.Forms.ToolStripMenuItem();
            this.menuItemSleep = new System.Windows.Forms.ToolStripMenuItem();
            this.menuItemHibernate = new System.Windows.Forms.ToolStripMenuItem();
            this.menuItemLock = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator1 = new System.Windows.Forms.ToolStripSeparator();
            this.menuItemExit = new System.Windows.Forms.ToolStripMenuItem();
            this.btnLock = new System.Windows.Forms.Button();
            this.btnHibernate = new System.Windows.Forms.Button();
            this.btnSleep = new System.Windows.Forms.Button();
            this.btnReboot = new System.Windows.Forms.Button();
            this.btnShutdown = new System.Windows.Forms.Button();
            this.contextMenuStrip.SuspendLayout();
            this.SuspendLayout();
            // 
            // notifyIcon
            // 
            this.notifyIcon.Text = "PowerSharp";
            this.notifyIcon.Visible = true;
            // 
            // contextMenuStrip
            // 
            this.contextMenuStrip.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.MenuItemShutdown,
            this.menuItemReboot,
            this.menuItemSleep,
            this.menuItemHibernate,
            this.menuItemLock,
            this.toolStripSeparator1,
            this.menuItemExit});
            this.contextMenuStrip.Name = "contextMenuStrip";
            this.contextMenuStrip.Size = new System.Drawing.Size(181, 164);
            // 
            // MenuItemShutdown
            // 
            this.MenuItemShutdown.Image = global::ShutSarp.Properties.Resources.system_shutdown;
            this.MenuItemShutdown.Name = "MenuItemShutdown";
            this.MenuItemShutdown.Size = new System.Drawing.Size(180, 22);
            this.MenuItemShutdown.Text = "Shutdown";
            this.MenuItemShutdown.Click += new System.EventHandler(this.ShutdownInit);
            // 
            // menuItemReboot
            // 
            this.menuItemReboot.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.menuItemReboot.Image = global::ShutSarp.Properties.Resources.system_reboot;
            this.menuItemReboot.Name = "menuItemReboot";
            this.menuItemReboot.Size = new System.Drawing.Size(180, 22);
            this.menuItemReboot.Text = "Reboot";
            this.menuItemReboot.Click += new System.EventHandler(this.RebootInit);
            // 
            // menuItemSleep
            // 
            this.menuItemSleep.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.menuItemSleep.Image = global::ShutSarp.Properties.Resources.system_suspend;
            this.menuItemSleep.Name = "menuItemSleep";
            this.menuItemSleep.Size = new System.Drawing.Size(180, 22);
            this.menuItemSleep.Text = "Sleep";
            this.menuItemSleep.Click += new System.EventHandler(this.SleepInit);
            // 
            // menuItemHibernate
            // 
            this.menuItemHibernate.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.menuItemHibernate.Image = global::ShutSarp.Properties.Resources.system_suspend_hibernate;
            this.menuItemHibernate.Name = "menuItemHibernate";
            this.menuItemHibernate.Size = new System.Drawing.Size(180, 22);
            this.menuItemHibernate.Text = "Hibernate";
            this.menuItemHibernate.Click += new System.EventHandler(this.HibernateInit);
            // 
            // menuItemLock
            // 
            this.menuItemLock.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.menuItemLock.Image = global::ShutSarp.Properties.Resources.system_lock_screen;
            this.menuItemLock.Name = "menuItemLock";
            this.menuItemLock.Size = new System.Drawing.Size(180, 22);
            this.menuItemLock.Text = "Lock";
            this.menuItemLock.Click += new System.EventHandler(this.LockInit);
            // 
            // toolStripSeparator1
            // 
            this.toolStripSeparator1.Name = "toolStripSeparator1";
            this.toolStripSeparator1.Size = new System.Drawing.Size(177, 6);
            // 
            // menuItemExit
            // 
            this.menuItemExit.Image = global::ShutSarp.Properties.Resources.application_exit;
            this.menuItemExit.Name = "menuItemExit";
            this.menuItemExit.Size = new System.Drawing.Size(180, 22);
            this.menuItemExit.Text = "Exit";
            this.menuItemExit.Click += new System.EventHandler(this.menuItemExit_Click);
            // 
            // btnLock
            // 
            this.btnLock.BackgroundImage = global::ShutSarp.Properties.Resources._lock;
            this.btnLock.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.btnLock.Location = new System.Drawing.Point(332, 12);
            this.btnLock.Name = "btnLock";
            this.btnLock.Size = new System.Drawing.Size(74, 88);
            this.btnLock.TabIndex = 4;
            this.btnLock.Text = "Lock";
            this.btnLock.TextAlign = System.Drawing.ContentAlignment.BottomCenter;
            this.btnLock.UseVisualStyleBackColor = true;
            this.btnLock.Click += new System.EventHandler(this.LockInit);
            // 
            // btnHibernate
            // 
            this.btnHibernate.BackgroundImage = global::ShutSarp.Properties.Resources.hibernate;
            this.btnHibernate.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.btnHibernate.Location = new System.Drawing.Point(252, 12);
            this.btnHibernate.Name = "btnHibernate";
            this.btnHibernate.Size = new System.Drawing.Size(74, 88);
            this.btnHibernate.TabIndex = 3;
            this.btnHibernate.Text = "Hibernate";
            this.btnHibernate.TextAlign = System.Drawing.ContentAlignment.BottomCenter;
            this.btnHibernate.UseVisualStyleBackColor = true;
            this.btnHibernate.Click += new System.EventHandler(this.HibernateInit);
            // 
            // btnSleep
            // 
            this.btnSleep.BackgroundImage = global::ShutSarp.Properties.Resources.sleep;
            this.btnSleep.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.btnSleep.Location = new System.Drawing.Point(172, 12);
            this.btnSleep.Name = "btnSleep";
            this.btnSleep.Size = new System.Drawing.Size(74, 88);
            this.btnSleep.TabIndex = 2;
            this.btnSleep.Text = "Sleep";
            this.btnSleep.TextAlign = System.Drawing.ContentAlignment.BottomCenter;
            this.btnSleep.UseVisualStyleBackColor = true;
            this.btnSleep.Click += new System.EventHandler(this.SleepInit);
            // 
            // btnReboot
            // 
            this.btnReboot.BackgroundImage = global::ShutSarp.Properties.Resources.reboot;
            this.btnReboot.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.btnReboot.Location = new System.Drawing.Point(92, 12);
            this.btnReboot.Name = "btnReboot";
            this.btnReboot.Size = new System.Drawing.Size(74, 88);
            this.btnReboot.TabIndex = 1;
            this.btnReboot.Text = "Reboot";
            this.btnReboot.TextAlign = System.Drawing.ContentAlignment.BottomCenter;
            this.btnReboot.UseVisualStyleBackColor = true;
            this.btnReboot.Click += new System.EventHandler(this.RebootInit);
            // 
            // btnShutdown
            // 
            this.btnShutdown.BackgroundImage = global::ShutSarp.Properties.Resources.shutdown;
            this.btnShutdown.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.btnShutdown.Location = new System.Drawing.Point(12, 12);
            this.btnShutdown.Name = "btnShutdown";
            this.btnShutdown.Size = new System.Drawing.Size(74, 88);
            this.btnShutdown.TabIndex = 0;
            this.btnShutdown.Text = "Shutdown";
            this.btnShutdown.TextAlign = System.Drawing.ContentAlignment.BottomCenter;
            this.btnShutdown.UseVisualStyleBackColor = true;
            this.btnShutdown.Click += new System.EventHandler(this.ShutdownInit);
            // 
            // MainForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            this.ClientSize = new System.Drawing.Size(419, 113);
            this.Controls.Add(this.btnLock);
            this.Controls.Add(this.btnHibernate);
            this.Controls.Add(this.btnSleep);
            this.Controls.Add(this.btnReboot);
            this.Controls.Add(this.btnShutdown);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "MainForm";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "PowerSharp";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.MainForm_FormClosing);
            this.contextMenuStrip.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Button btnShutdown;
        private System.Windows.Forms.Button btnReboot;
        private System.Windows.Forms.Button btnSleep;
        private System.Windows.Forms.Button btnHibernate;
        private System.Windows.Forms.Button btnLock;
        private System.Windows.Forms.NotifyIcon notifyIcon;
        private System.Windows.Forms.ContextMenuStrip contextMenuStrip;
        private System.Windows.Forms.ToolStripMenuItem MenuItemShutdown;
        private System.Windows.Forms.ToolStripMenuItem menuItemReboot;
        private System.Windows.Forms.ToolStripMenuItem menuItemSleep;
        private System.Windows.Forms.ToolStripMenuItem menuItemHibernate;
        private System.Windows.Forms.ToolStripMenuItem menuItemLock;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator1;
        private System.Windows.Forms.ToolStripMenuItem menuItemExit;
    }
}

